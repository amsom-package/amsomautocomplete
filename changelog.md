# Changelog

<!-- introduction au changelog -->

## V1.2.0 - 19/02/2025

### Added

- `placeholder` : Placeholder de l'input. Default `Recherchez...`
- `loading` : Ajoute un AmsomOverlay pour un chargement. Boolean default `false`
- `zIndex`: Permet d'ajuster le z-index de la liste affichée. Default `1000`

### Fixed

- Text wrapped pour désactiver l'horizontal overflow

## V1.1.1 - 06/08/2024

### Added

- MaxHeightResults pour definir un scroll sur les résultats

### Fixed

- Correction du css

## V1.0.0 - 28/05/2024

### Added

- Init projet

## V0.0.0 - 05/03/2023

### Added

- blabla
- blabla

### Fixed

- blabla
- blabla

### Changed

- blabla
- blabla

### Removed

- blabla
- blabla
