# AMSOM AutocompleteCity

Ce package propose un composant d'autocomplete

## Installation

```bash
npm i @amsom-habitat/amsom-autocomplete
```

Importer les css dans le main.js tel que :

```javascript
import '@amsom-habitat/amsom-autocomplete/dist/style.css'
```

Pour utiliser le package:

```javascript
import { AmsomAutocomplete } from '@amsom-habitat/amsom-autocomplete'
```

## Développment

Après avoir fait vos dev, veillez à bien tenir à jour le [changelog.md](changelog.md) ainsi que la version du package.json puis faites :

```bash
git add .
git commit -m '<commentaire'
git push origin <branch>
```

## Tests

Les tests sont réalisé de manière automatique sur les branches main et dev mais peuvent être fait localement, notemment pour voir l'evolution du développement via la commande :

```bash
npm run storybook
```

Le valideur devra, si des changements sont observés, aller sur la pipeline pour valider les différences à l'aide de chromatic, sans cela aucun merge-request ne sera possible. Si un merge est effectué, une double verification sera necessaire.

## Déploiement

Après avoir merge les dev sur la branche main, exécutez :

```bash
make publish
```

Cette commande vérifie la version, le changelog et publie le tout

## Utilisation

#### Props

- `v-model` : La valeur de l'input (doit etre une instance de items)
- `items` : Les items à afficher
- `getInputLibelle` : La fonction qui permet de récupérer le libellé de l'item, par defaut récupère item.name
- `identifier` : L'identifiant de l'input
- `@search` : Emit l'input de recherche
- `maxResultsHeight` : La hauteur max des résultats
- `placeholder` : Placeholder de l'input. Default `Recherchez...`
- `loading` : Ajoute un AmsomOverlay pour un chargement. Boolean default `false`
- `zIndex`: Permet d'ajuster le z-index de la liste affichée. Default `1000`

#### Example complet

```tsx
<tempate>
  <amsom-autocomplete :items="filteredItems" v-model="selection" @search="searchItem" maxResultsHeight="10vh"/>
</template>

<script>
import '@amsom-habitat/amsom-autocomplete/dist/style.css'
import {AmsomAutocomplete} from '@amsom-habitat/amsom-autocomplete'

  export default {
    name: 'TestPage',
    components: {
      AmsomAutocomplete,
    },
    data() {
        return {
          items: [
            {id: 1, name: 'item 1'},
            {id: 2, name: 'item 2'},
            {id: 3, name: 'item 3'},
            {id: 4, name: 'item 4'},
            {id: 5, name: 'item 5'},
          ],
          selection: null,
          search: '',
      }
    },
    computed: {
        filteredItems(search) {
          if(!search)
          return this.items

          return this.items.filter(item => item.name.toLowerCase().includes(this.search.toLowerCase()))
        },
    },
    methods: {
      searchItem(search) {
        this.search = search
      },
    }
}
</script>
```
