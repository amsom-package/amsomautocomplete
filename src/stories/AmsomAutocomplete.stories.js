import AmsomAutocomplete from '@/AmsomAutocomplete.vue'
import { onMounted, ref } from 'vue'

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories
export default {
  components: { AmsomAutocomplete },
  title: 'AmsomAutocomplete',
  component: AmsomAutocomplete,
  tags: ['autodocs'],
  argTypes: {
  },
};


const Template = (args) => ({
  components: { AmsomAutocomplete },
  setup() {
    const baseItems = [
      { id: 1, name: 'Item 1' },
      { id: 2, name: 'Item 2' },
      { id: 3, name: 'Item 3' },
      { id: 4, name: 'Item 4' },
      { id: 5, name: 'Item 5' },
    ];
    const items = ref(args.items);
    items.value = args.items ?? baseItems;
    const onSearch = (value) => {
      items.value = baseItems.filter(item => item.name.toLowerCase().includes(value.toLowerCase()));
    };
    onMounted(() => {
      if(args.focus){
        const input = document.querySelector('input');
        if (input) {
          input.focus();
        }
      }
    });
    return { args, items, onSearch };
  },
  template: '<amsom-autocomplete v-bind="args" :items="items" @search="onSearch" /> test',
});

const TemplateSmallSize = (args) => ({
  components: { AmsomAutocomplete },
  setup() {
    const baseItems = [
      { id: 1, name: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur venenatis eros dui, id tristique risus vehicula nec. In hac habitasse platea dictumst. Etiam quis varius metus. Integer consectetur, tortor a convallis sodales, felis nulla consectetur orci, quis tempus dui augue nec tortor. Suspendisse lacinia, justo non sodales convallis, tellus ligula dictum lectus, quis pellentesque massa lacus id ante. Donec efficitur mi at tellus interdum, vulputate egestas lacus dictum. Proin congue velit tellus, non ullamcorper dolor suscipit vitae.' },
      { id: 2, name: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur venenatis eros dui, id tristique risus vehicula nec. In hac habitasse platea dictumst. Etiam quis varius metus. Integer consectetur, tortor a convallis sodales, felis nulla consectetur orci, quis tempus dui augue nec tortor. Suspendisse lacinia, justo non sodales convallis, tellus ligula dictum lectus, quis pellentesque massa lacus id ante. Donec efficitur mi at tellus interdum, vulputate egestas lacus dictum. Proin congue velit tellus, non ullamcorper dolor suscipit vitae.' },
    ];

    const items = ref(args.items);
    items.value = args.items ?? baseItems;
    const onSearch = (value) => {
      items.value = baseItems.filter(item => item.name.toLowerCase().includes(value.toLowerCase()));
    };
    onMounted(() => {
      if(args.focus){
        const input = document.querySelector('input');
        if (input) {
          input.focus();
        }
      }
    });
    return { args, items, onSearch };
  },
  template: '<div style="width: 200px;"><amsom-autocomplete v-bind="args" :items="items" @search="onSearch" /> test </div>'
});

export const Default = Template.bind({});
Default.args = {
};

export const NoResults = Template.bind({});
NoResults.args = {
  items: [],
};

export const SelectionPredefined = Template.bind({});
SelectionPredefined.args = {
  modelValue: { id: 2, name: 'Item 2' },
};

export const WrongSelectionPredefined = Template.bind({});
WrongSelectionPredefined.args = {
  modelValue: { id: 3, name: 'Item 2' },
};

export const MaxHeight = Template.bind({});
MaxHeight.args = {
  maxResultsHeight: "15vh",
};

export const DefaultFocused = Template.bind({});
DefaultFocused.args = {
  focus: true,
};

export const NoResultsFocused = Template.bind({});
NoResultsFocused.args = {
  items: [],
  focus: true,
};

export const MaxHeightFocused = Template.bind({});
MaxHeightFocused.args = {
  focus: true,
  maxResultsHeight: "15vh"
};

export const SelectionPredefinedFocused = Template.bind({});
SelectionPredefinedFocused.args = {
  focus: true,
  modelValue: { id: 2, name: 'Item 2' },
};

export const WrongSelectionPredefinedFocused = Template.bind({});
WrongSelectionPredefinedFocused.args = {
  focus: true,
  modelValue: { id: 3, name: 'Item 2' },
};

export const Placeholder = Template.bind({});
Placeholder.args = {
  placeholder: "Je suis un nouveau placeholder",
};

export const Loading = Template.bind({});
Loading.args = {
  loading: true,
  focus: true,
}; 

export const TextWrapped = TemplateSmallSize.bind({});
TextWrapped.args = {
  focus: true,
}; 