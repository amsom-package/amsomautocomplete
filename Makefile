git_add_version:
	VERSION=$$(grep -o '"version": "[^"]*' package.json | grep -o '[^"]*$$') && echo $$VERSION\
	&& git tag -a $$VERSION -m "version $${VERSION} from ci-cd" && git push origin $$VERSION

publish:
	git checkout main && git pull origin main && npm i && npm run lint && npm run build && ./check-version.sh && make git_add_version && npm publish --access public

alpha_publish:
	git checkout dev && git pull origin dev && npm i && npm run lint && npm run build && npm version prerelease --preid=beta && npm publish --access public
