#!/bin/bash

# Obtenir la version actuelle du module npm
current_version=$(grep -o '"version": "[^"]*' package.json | grep -o '[^"]*$')

# Recupération de la version majeur et de la version mineur
major_version=$(echo $current_version | cut -d. -f1)
minor_version=$(echo $current_version | cut -d. -f2)

# on verifie si on a un -alpha ou -beta ou autre et on rejete la version si c'est le cas
if [[ $current_version == *-* ]]; then
  echo "La version actuelle est une version alpha, beta ou autre."
  exit 1
fi

# regex pour verifier si on a bien la chaine "[X.Y.Z]" avec X major_version, Y minor_version et Z n'importe que ce soit des chiffres ou des lettres
regex="^## V$major_version\.$minor_version\.[0-9]"
# on applique la regex sur le changelog
if ! grep -q "$regex" changelog.md; then
  echo "La version $major_version.$minor_version n'existe pas dans le changelog."
  exit 1
fi

echo "La version et la note de patch sont correctes."
